import json from '../apis/json_request.js';

export const fetchPosts = () => {
    return (dispatch, getState) => {
        json.get('/posts')
            .then((response) => {
                dispatch({ type: 'FETCH_POSTS', payload: response.data });
            });
    }
};

const fetching = [];
export const fetchUser = (id) => {
    if(fetching.indexOf(id) == -1){
        fetching.push(id);
        return (dispatch, getState) => 
            json.get(`/users/${id}`)
                .then((response) => {
                    dispatch({ type: 'FETCH_USER', payload: { id: id, data: response.data } });
                });
    }else return { type: '', payload: null };
};