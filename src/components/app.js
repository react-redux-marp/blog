import React from 'react';
import PostList from './post-list.js'

class App extends React.Component{
    render(){
        return (
            <div>
                <PostList />
            </div>
        );
    }
}

export default App;