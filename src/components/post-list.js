import React from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';
import UserHeader from './user-header.js';

class PostList extends React.Component{
    componentDidMount(){
        this.props.fetchPosts();
    }
    renderPost(post){
        return (
            <div className="post item col-12" id={post.id} key={post.id} data-author={post.userId}>
                <i className="large middle aligned icon user" />
                <div className="content">
                    <h3>{post.title}</h3>
                    <p>{post.body}</p>
                </div>
                <UserHeader userId={post.userId}/>
            </div>
        );
    }
    render(){
        let posts = this.props.posts.map((post) => {
            return this.renderPost(post);
        });
        return (
            <div className="container">
                <div className="row">
                    {posts}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { posts: state.posts };
}

export default connect(mapStateToProps, {
    fetchPosts: fetchPosts
})(PostList);