import React from 'react';
import { connect } from 'react-redux';
import { fetchUser } from '../actions';

class UserHeader extends React.Component{
    componentDidMount(){
        this.props.fetchUser(this.props.userId);
    }
    render(){
        return (
        <div>{this.props.user.name}, {this.props.user.email}</div>
        );
    }
}
UserHeader.fetchedUsers = [];

function mapStateToProps(state, props){
    return {
        user: state.users[`${props.userId}`] || {}
    };
}

export default connect(mapStateToProps, {
    fetchUser: fetchUser
})(UserHeader);