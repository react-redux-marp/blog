import { combineReducers } from 'redux';

export default combineReducers({
    'posts': (posts = [], action) => {
        if(action.type == "FETCH_POSTS"){
            return action.payload;
        }
        return posts;
    },
    'users': (users = {}, action) => {
        if(action.type == "FETCH_USER" && !users[`${action.payload.id}`]){
            let new_users = {...users};
            new_users[`${action.payload.id}`] = action.payload.data;
            return new_users;
        }
        return users;
    }
});